<?php
declare(strict_types=1);

namespace G3\SymfonyProject\Application;

use G3\SymfonyProject\Domain\LogSummary;


final class LogSummaryGetter
{
    private $logEntryArray;
    private $levelsRaw;

    public function __construct(array $logEntryArray, string $levelsRaw)
    {
        $this->logEntryArray = $logEntryArray;
        $this->levelsRaw     = $levelsRaw;
    }

    public function __invoke(): array
    {
        $filteredLogs = $this->filterLog();
        return $this->summaryLog($filteredLogs);
    }

    private function getEnteredLevels(string $enteredLevels): array
    {
        $lowCaseLevels = strtoupper($enteredLevels);
        $levels        = explode(",", $lowCaseLevels);
        return $levels;
    }

    private function filterLog(): array
    {
        $filteredLog = [];
        $levels      = $this->getEnteredLevels($this->levelsRaw);
        foreach ($this->logEntryArray as $log) {
            foreach ($levels as $level) {
                if ($log->levelName() === $level) {
                    $filteredLog[] = $log;
                }
            }
        }

        return $filteredLog;
    }

    private function summaryLog($filteredLogs): array
    {
        $summary = new LogSummary();
        foreach ($filteredLogs as $filterLog) {
            $summary->increaseCountLevel($filterLog->levelName());
        }

        return $summary->getSummary();
    }
}
