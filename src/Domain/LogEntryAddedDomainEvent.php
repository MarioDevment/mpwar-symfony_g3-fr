<?php

declare(strict_types=1);

namespace G3\SymfonyProject\Domain;

final class LogEntryAddedDomainEvent implements DomainEvent
{
    private $level;

    public function __construct(string $level)
    {
        $this->level = $level;
    }
}