<?php
declare(strict_types=1);

namespace G3\SymfonyProject\Domain;


final class LogSummary
{
    private $summary;

    public function __construct()
    {
        $this->summary = [];
    }

    public function increaseCountLevel(string $level)
    {
        if (array_key_exists($level, $this->summary)) {
            $this->summary[$level] = $this->summary[$level] + 1;
        } else {
            $this->summary[$level] = 1;
        }
    }

    public function getSummary()
    {
        return $this->summary;
    }
}
