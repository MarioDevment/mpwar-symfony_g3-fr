<?php
declare(strict_types=1);

namespace G3\SymfonyProject\Infrastructure\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Hello extends Command
{
    protected function configure(): void
    {
        $this
            ->setName('message:hello')
            ->setDescription('Hello World')
            ->setHelp('say Hello World...');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln("Hello World");
    }
}