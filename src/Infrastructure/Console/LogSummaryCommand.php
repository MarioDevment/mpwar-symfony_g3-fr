<?php
declare(strict_types=1);

namespace G3\SymfonyProject\Infrastructure\Console;

use G3\SymfonyProject\Application\LogSummaryGetter;
use G3\SymfonyProject\Domain\LogEntry;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

final class LogSummaryCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->setName('log:summary')
            ->setDescription('Read and print last file')
            ->setHelp('Print last log file')
            ->addArgument(
                'environment',
                InputArgument::OPTIONAL,
                '[string] Enter a environment to show. Example: "dev"'
            )
            ->addArgument('levels', InputArgument::OPTIONAL, '[string] Enter levels to show. Example: "warning,error"');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $environment = $this->getEnvironment($input, $output);
        $levels      = $this->getLevels($input, $output);

        $currentLogFile = $this->getURLFile($environment);
        $logEntryArray  = $this->getContentFile($currentLogFile);

        $logSummaryGetter = new LogSummaryGetter($logEntryArray, $levels);
        $summaryLog       = $logSummaryGetter->__invoke();
        $this->print($summaryLog, $output);
    }

    private function print(array $summaryLog, OutputInterface $output): void
    {
        foreach ($summaryLog as $key => $value) {
            $output->writeln($key . ": " . $value);
        }
    }

    private function getContentFile(string $currentFile): array
    {
        if (file_exists($currentFile)) {
            $file     = file($currentFile, FILE_SKIP_EMPTY_LINES);
            $logEntry = $this->serializer($file);

            return $logEntry;
        }

        throw new Exception(
            sprintf('The file log, don\'t exits')
        );
    }

    private function serializer(array $fileContents): array
    {
        $metadataFactory                   = null;
        $camelCaseToSnakeCaseNameConverter = new CamelCaseToSnakeCaseNameConverter();

        $normalizers = [new ObjectNormalizer($metadataFactory, $camelCaseToSnakeCaseNameConverter)];
        $encoders    = [new JsonEncoder()];
        $serializer  = new Serializer($normalizers, $encoders);

        return $this->getLineContent($fileContents, $serializer);
    }

    private function getLineContent(array $arrayContent, Serializer $serializer): array
    {
        $logging = [];
        foreach ($arrayContent as $line) {
            $data      = str_replace('/', '\/', $line);
            $logging[] = $serializer->deserialize(
                $data,
                LogEntry::class,
                'json',
                ['allow_extra_attributes' => false]
            );
        }

        return $logging;
    }

    private function getURLFile(string $enteredEnvironment): string
    {
        $today       = date("Y-m-d");
        $environment = $this->defineEnvironment($enteredEnvironment);
        return 'var/log/' . $environment . '-' . $today . '.json';
    }

    private function defineEnvironment(string $enteredEnvironment): string
    {
        if ($enteredEnvironment == "prod" || $enteredEnvironment == "dev") {
            $environment = $enteredEnvironment;
        } else {
            $environment = $_SERVER['APP_ENV'] ?? 'dev';
        }
        return $environment;
    }

    private function getEnvironment(InputInterface $input, OutputInterface $output): string
    {
        $helper             = $this->getHelper('question');
        $enteredEnvironment = $input->getArgument('environment');
        if (empty($enteredEnvironment)) {
            $question           = new Question('Please enter a environment to show: ', 'dev');
            $enteredEnvironment = $helper->ask($input, $output, $question);
        }
        return $enteredEnvironment;
    }

    private function getLevels(InputInterface $input, OutputInterface $output): string
    {
        $helper        = $this->getHelper('question');
        $enteredLevels = $input->getArgument('levels');
        if (empty($enteredLevels)) {
            $question      = new Question('Please enter levels to show separated by comma: ', 'dev,info,warning,error');
            $enteredLevels = $helper->ask($input, $output, $question);
        }
        $enteredLevelNotSpaces = str_replace(' ', '', $enteredLevels);
        return $enteredLevelNotSpaces;
    }
}
