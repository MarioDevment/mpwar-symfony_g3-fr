<?php

namespace G3\SymfonyProject\Infrastructure\Console;

use Closure;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

final class ServiceSummary extends Command
{
    protected function configure(): void
    {
        $this
            ->setName('service:summary')
            ->setDescription('Read and print active services');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $container        = $this->getContainerBuilder();
        $allServicesClass = $this->filterServiceByTag($container);
        dump($allServicesClass);
    }

    private function getContainerBuilder(): ContainerBuilder
    {
        $kernel          = $this->getApplication()->getKernel();
        $kernelContainer = $kernel->getContainer()->getParameter('debug.container.dump');
        $debug           = true;

        $kernelDebug = new ConfigCache($kernelContainer, $debug);

        if (!$kernel->isDebug() || !($kernelDebug)->isFresh()) {
            $buildContainer = $this->getBuildContainer($kernel);
            $container      = $buildContainer();
            $container->getCompilerPassConfig()->setRemovingPasses([]);
            $container->compile();
        } else {
            $container     = new ContainerBuilder();
            $fileLocator   = new FileLocator();
            $xmlFileLoader = new XmlFileLoader($container, $fileLocator);
            $xmlFileLoader->load($kernel->getContainer()->getParameter('debug.container.dump'));

        }

        return $container;
    }

    private function getBuildContainer($kernel): Closure
    {
        $buildContainer = Closure::bind(function () {
            return $this->buildContainer();
        }, $kernel, get_class($kernel));
        return $buildContainer;
    }

    public function filterServiceByTag(ContainerBuilder $builder): array
    {
        $serviceIds        = $builder->findTaggedServiceIds('g3.use_case');
        $foundServiceClass = [];
        foreach ($serviceIds as $serviceClass => $serviceId) {
            $foundServiceClass[] = $serviceClass;
        }

        return $foundServiceClass;
    }
}