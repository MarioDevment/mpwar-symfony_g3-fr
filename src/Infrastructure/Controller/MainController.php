<?php
declare(strict_types=1);

namespace G3\SymfonyProject\Infrastructure\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

final class MainController extends Controller
{
    private $environment;
    private $environmentName;
    private $logger;

    public function __construct(string $environment, string $environmentName, LoggerInterface $logger)
    {
        $this->environment = $environment;
        $this->environmentName = $environmentName;
        $this->logger = $logger;
    }

    public function showHelloEnv(Request $request)
    {
        $this->logger->info('InfoLogger');
        $this->logger->warning('WarningLogger');

        if ($request->query->has('bum')) {
            $this->logger->error('ErrorLogger', ["hello" => "world"]);
        }

        return $this->render('hello.html.twig', [
            'ENV' => $this->environment
        ]);
    }

    public function showHelloCustom()
    {
        return $this->render('hello.html.twig', [
            'ENV' => $this->environmentName
        ]);
    }
}