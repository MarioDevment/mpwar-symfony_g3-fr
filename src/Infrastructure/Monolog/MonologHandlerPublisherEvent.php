<?php
declare(strict_types=1);

namespace G3\SymfonyProject\Infrastructure\Monolog;

use G3\SymfonyProject\Domain\LogEntryAddedDomainEvent;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class MonologHandlerPublisherEvent extends AbstractProcessingHandler
{
    private $dispatcher;

    public function __construct($level = Logger::DEBUG, $bubble = true, EventDispatcherInterface $dispatcher)
    {
        parent::__construct($level, $bubble);
        $this->dispatcher = $dispatcher;
    }

    protected function write(array $record)
    {
        $event = new LogEntryAddedDomainEvent($record['level']);

        $this->dispatcher->dispatch('log_record.locally_raised', $event);
    }
}